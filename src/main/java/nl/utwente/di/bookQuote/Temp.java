package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that converts Temperature to glorious (REAL RAW) American Standards
 */

public class Temp extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TempCalc tempCalc;
	
    public void init() throws ServletException {
    	tempCalc = new TempCalc();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Temperature to glorious (REAL RAW) American Standards";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature in Celsius: " +
                   request.getParameter("temp") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                   Double.toString(tempCalc.celsiusToF(request.getParameter("temp"))) +
                "</BODY></HTML>");
  }
  

}

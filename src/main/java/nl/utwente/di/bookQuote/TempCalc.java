package nl.utwente.di.bookQuote;

public class TempCalc {
    public double celsiusToF(String tempCelsius) {
        double celsius = Double.parseDouble(tempCelsius);
        return celsius * 1.8 + 32;
    }
}
